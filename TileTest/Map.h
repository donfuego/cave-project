//
//  Map.h
//  TileTest
//
//  Created by Joel Ricci on 2/19/14.
//  Copyright (c) 2014 Joel. All rights reserved.
//

#import "CCNode.h"


#define RAND_ZERO_TO_ONE (arc4random() % ((unsigned)RAND_MAX + 1) / RAND_MAX)
#define RANDOM(min, max) (min + RAND_ZERO_TO_ONE * (max-min))

#define MIN_LEAF_SIZE 6
#define MAX_LEAF_SIZE 20


@interface Map : CCNode

@property (nonatomic) CGSize gridSize;
@property (nonatomic, readonly) CGPoint spawnPoint;
@property (nonatomic, readonly) CGPoint exitPoint;

+(instancetype) mapWithGridSize:(CGSize)gridSize;
-(instancetype) initWithGridSize:(CGSize)gridSize;
-(void)generateTileMap;

@end


// BSP Leaf node
@interface Leaf : NSObject {

//    int x, y, width, height;
//    Leaf *leftChild, *rightChild;
    CGRect room;
    NSArray *halls;
    
}

@property (nonatomic) Leaf *leftChild, *rightChild;
@property (nonatomic) CGRect rectangle;
@property (nonatomic) int x, y, width, height;

+(instancetype)leafWithRect:(CGRect)rect;
-(instancetype)initWithRect:(CGRect)rect;
-(BOOL)split;
-(BOOL)createRooms;

@end
