//
//  main.m
//  TileTest
//
//  Created by Joel on 2013-07-30.
//  Copyright Joel 2013. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, @"AppController");
        return retVal;
    }
}
