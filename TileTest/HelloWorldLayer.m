//
//  HelloWorldLayer.m
//  TileTest
//
//  Created by Joel on 2013-07-30.
//  Copyright Joel 2013. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#pragma mark - HelloWorldLayer

@interface HelloWorldLayer()

@property(strong) CCTMXTiledMap *tileMap;
@property(strong) CCTMXLayer *background;
@property(strong) CCTMXLayer *meta;
@property(strong) CCTMXLayer *foreground;
@property(strong) CCSprite *player;

@end

// HelloWorldLayer implementation
@implementation HelloWorldLayer {
    CGPoint lastKnownGoodPosition;
}

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
		self.touchEnabled = YES;
        
		self.tileMap = [CCTMXTiledMap tiledMapWithTMXFile:@"test.tmx"];
        self.background = [_tileMap layerNamed:@"Background"];
        self.meta = [_tileMap layerNamed:@"Meta"];
        self.foreground = [_tileMap layerNamed:@"Foreground"];
        _meta.visible = NO;
        
        CCTMXObjectGroup *objectGroup = [_tileMap objectGroupNamed:@"Objects"];
        NSAssert(objectGroup != nil, @"tile map is missing objects layer!");
        
        NSDictionary *spawnPoint = [objectGroup objectNamed:@"SpawnPoint"];
        int x = [spawnPoint[@"x"] integerValue];
        int y = [spawnPoint[@"y"] integerValue];
        
        _player = [CCSprite spriteWithFile:@"Player48.png"];
        _player.position = ccp(x,y);
//        _player.anchorPoint = ccp(0.2, 0.25);
        
        [self addChild:_player];
        [self setViewPointCenter:_player.position];
        [self addChild:_tileMap z:-1];
        
        // Make the camera follow the player
        CGRect worldBoundary = CGRectMake(0, 0, _tileMap.boundingBox.size.width, _tileMap.boundingBox.size.height);
        [self runAction:[CCFollow actionWithTarget:_player worldBoundary:worldBoundary]];
        
        lastKnownGoodPosition = _player.position;
        
        Map *myMap = [Map mapWithGridSize:CGSizeMake(_tileMap.boundingBox.size.width, _tileMap.boundingBox.size.height)];
        [myMap generateTileMap];
	}
	return self;
}

-(CGPoint)tileCoordForPosition:(CGPoint)position
{
    int x = position.x / _tileMap.tileSize.width;
    //y axis in tilemap is inverted compared to screen, so:
    int y = ((_tileMap.mapSize.height * _tileMap.tileSize.height) - position.y) / _tileMap.tileSize.height;
    return  ccp(x,y);
}


-(void) setViewPointCenter:(CGPoint) position
{
//    CGSize winSize = [CCDirector sharedDirector].winSize;
    
//    int x = MAX(position.x, winSize.width/2);
//    int y = MAX(position.y, winSize.height/2);
//    x = MIN(x, (_tileMap.mapSize.width * _tileMap.tileSize.width) - winSize.width/2);
//    y = MIN(y, (_tileMap.mapSize.height * _tileMap.tileSize.height) - winSize.height/2);
//    CGPoint actualPosition = ccp(x, y);
//    
//    CGPoint centerOfView = ccp(winSize.width/2, winSize.height/2);
//    CGPoint viewPoint = ccpSub(centerOfView, actualPosition);
//    self.position = viewPoint;
}

#pragma mark - handle touches
-(void) registerWithTouchDispatcher
{
    [[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    return YES;
}

-(void) setPlayerPosition:(CGPoint) position
{
//    _player.position = position;
    
    float distance = ccpDistance(_player.position, position);
    
    float duration = distance / (150.0/1.0); // pixels per second
    
    [_player stopAllActions];
    
    [_player runAction:[CCMoveTo actionWithDuration:duration position:position]];

}

-(void)stopPlayerMovement {
    [_player stopAllActions];
}

-(NSDictionary *)getTilePropertyForCoord:(CGPoint)tileCoord
{
    int tileGid = [_meta tileGIDAt:tileCoord];
//    NSLog(@"%i", tileGid);
    if (tileGid) {
        NSDictionary *props = [_tileMap propertiesForGID:tileGid];
        NSLog(@"%@", props);
        return props;
    }
    
    return nil;
}


-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint touchLocation = [touch locationInView:touch.view];
    touchLocation = [[CCDirector sharedDirector] convertToGL:touchLocation];
    touchLocation = [self convertToNodeSpace:touchLocation];
    
    CGPoint playerPos = _player.position;
    CGPoint diff = ccpSub(touchLocation, playerPos);
    
    if ( abs(diff.x) > abs(diff.y) ) {
        if (diff.x > 0) {
            playerPos.x += _tileMap.tileSize.width;
        } else {
            playerPos.x -= _tileMap.tileSize.width;
        }        
    } else {        
        if (diff.y > 0) {
            playerPos.y += _tileMap.tileSize.height;
        } else {
            playerPos.y -= _tileMap.tileSize.height;
        }        
    }
 
    CCLOG(@"playerPos %@",CGPointCreateDictionaryRepresentation(playerPos));
    
    // safety check on the bounds of the map
    if (playerPos.x <= (_tileMap.mapSize.width * _tileMap.tileSize.width) &&
        playerPos.y <= (_tileMap.mapSize.height * _tileMap.tileSize.height) &&
        playerPos.y >= 0 &&
        playerPos.x >= 0 )
    {
        [self setPlayerPosition:touchLocation];
    }
 
//    [self setViewPointCenter:_player.position];
}

-(void) draw {
    [super draw];

    CGPoint tileCoord = [self tileCoordForPosition:_player.position];
    NSDictionary *props = [self getTilePropertyForCoord:tileCoord];
    
//    NSLog(@"prop = %@", props);
    
    if (props) {
        
        NSString *attrib = props[@"Collidable"];
        if (attrib && [attrib isEqualToString:@"True"]) {
            NSLog(@"collision");
            [self stopPlayerMovement];
            _player.position = lastKnownGoodPosition;
        }
        attrib = props[@"Collectable"];
        if (attrib && [attrib isEqualToString:@"True"]) {
            NSLog(@"collectable");
            [_meta removeTileAt:tileCoord];
            [_foreground removeTileAt:tileCoord];
        }
    }
 
    lastKnownGoodPosition = _player.position;
    
//    CCLOG(@"%@", CGPointCreateDictionaryRepresentation(_player.position));
}

// on "dealloc" you need to release all your retained objects

#pragma mark GameKit delegate

-(void) achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

-(void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}
@end
