//
//  Map.m
//  TileTest
//
//  Created by Joel Ricci on 2/19/14.
//  Copyright (c) 2014 Joel. All rights reserved.
//


#import "Map.h"

@implementation Map

+(instancetype)mapWithGridSize:(CGSize)gridSize
{
    return [[self alloc] initWithGridSize:gridSize];
}

-(instancetype)initWithGridSize:(CGSize)gridSize
{
    if (self = [super init])
    {
        CCLOG(@"Map init with gridSize %@", CGSizeCreateDictionaryRepresentation(gridSize));
        self.gridSize = gridSize;
        _spawnPoint = CGPointZero;
        _exitPoint = CGPointZero;
    }
    return self;
}

-(void)generateTileMap
{
    NSMutableArray *leafs = [NSMutableArray array];
    Leaf *l;
    Leaf *root = [Leaf leafWithRect:CGRectMake(0, 0, _gridSize.width, _gridSize.height)];
    
    [leafs addObject:root];
    
    BOOL did_split = YES;
    while (did_split)
    {
        CCLOG(@".");
        did_split = NO;
        for (l in leafs)
        {
            if (l.leftChild == nil && l.rightChild == nil) // if this Leaf is not already split...
            {
                // if this Leaf is too big, or 75% chance...
                if (l.width > MAX_LEAF_SIZE || l.height > MAX_LEAF_SIZE || RAND_ZERO_TO_ONE > 0.25)
                {
                    if ([l split]) // split the Leaf!
                    {
                        // if we did split, push the child leafs to the Vector so we can loop into them next
                        [leafs addObject:l.leftChild];
                        [leafs addObject:l.rightChild];
                        did_split = true;
                    }
                }
            }
        }
    }
    
    
}

@end


@implementation Leaf

+(instancetype)leafWithRect:(CGRect)rect
{
    return [[self alloc] initWithRect:rect];
}

-(instancetype)initWithRect:(CGRect)rect
{
    if (self = [super init]) {
        _rectangle = rect;
    }
    return self;
}

-(BOOL)split
{
    // begin splitting the leaf into two children
    if (_leftChild != nil || _rightChild != nil)
        return false; // we're already split! Abort!
    
    // determine direction of split
    // if the width is >25% larger than height, we split vertically
    // if the height is >25% larger than the width, we split horizontally
    // otherwise we split randomly
    BOOL splitH = arc4random() > 0.5;
    if (_width > _height && _height / _width >= 0.05)
        splitH = NO;
    else if (_height > _width && _width / _height >= 0.05)
        splitH = YES;
    
    int max = (splitH ? _height : _width) - MIN_LEAF_SIZE; // determine the maximum height or width
    if (max <= MIN_LEAF_SIZE)
        return false; // the area is too small to split any more...
    
    int split = RANDOM(MIN_LEAF_SIZE, max); // determine where we're going to split
    
    // create our left and right children based on the direction of the split
    if (splitH)
    {
        _leftChild = [self initWithRect:CGRectMake(_x, _y, _width, split)];
        _rightChild = [self initWithRect:CGRectMake(_x, _y + split, _width, _height - split)];
    }
    else
    {
        _leftChild = [self initWithRect:CGRectMake(_x, _y, split, _height)];
        _rightChild = [self initWithRect:CGRectMake(_x + split, _y, _width - split, _height)];
    }
    return true; // split successful!
}

-(BOOL)createRooms
{
    // begin splitting the leaf into two children
    if (_leftChild != nil || _rightChild != nil)
        return false; // we're already split! Abort!
    
    // determine direction of split
    // if the width is >25% larger than height, we split vertically
    // if the height is >25% larger than the width, we split horizontally
    // otherwise we split randomly
    BOOL splitH = RAND_ZERO_TO_ONE > 0.5;
    if (_width > _height && _height / _width >= 0.05)
        splitH = false;
    else if (_height > _width && _width / _height >= 0.05)
        splitH = true;
    
    int max = (splitH ? _height : _width) - MIN_LEAF_SIZE; // determine the maximum height or width
    if (max <= MIN_LEAF_SIZE)
        return false; // the area is too small to split any more...
    
    int split = RANDOM(MIN_LEAF_SIZE, max); // determine where we're going to split
    
    // create our left and right children based on the direction of the split
    if (splitH)
    {
        _leftChild = [self initWithRect:CGRectMake(_x, _y, _width, split)];
        _rightChild = [self initWithRect:CGRectMake(_x, _y + split, _width, _height - split)];
    }
    else
    {
        _leftChild = [self initWithRect:CGRectMake(_x, _y, split, _height)];
        _rightChild = [self initWithRect:CGRectMake(_x + split, _y, _width - split, _height)];
    }
    return true; // split successful!

}

@end