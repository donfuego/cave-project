//
//  HelloWorldLayer.h
//  TileTest
//
//  Created by Joel on 2013-07-30.
//  Copyright Joel 2013. All rights reserved.
//


#import <GameKit/GameKit.h>
#import "Map.h"

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

// HelloWorldLayer
@interface HelloWorldLayer : CCLayer <GKAchievementViewControllerDelegate, GKLeaderboardViewControllerDelegate>
{
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
